# Présentation du stockage K8s

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Intro

Nous allons parler du stockage dans Kubernetes. Cette leçon va fournir un aperçu rapide du stockage dans Kubernetes et de ce sur quoi nous allons nous concentrer dans cette section.

# Objectifs

1. **Systèmes de fichiers des conteneurs**
2. **Volumes**
3. **Volumes persistants**
4. **Types de volumes**

# Systèmes de fichiers des conteneurs

Pour comprendre le stockage dans Kubernetes, il est important de commencer par comprendre quelque chose sur le système de fichiers des conteneurs, à savoir que les systèmes de fichiers des conteneurs sont éphémères. Cela signifie que les fichiers situés directement dans le système de fichiers des conteneurs existent uniquement tant que le conteneur existe. 

Par exemple, si nous avons un pod avec un conteneur et des fichiers à l'intérieur de ce conteneur, dans le système de fichiers du conteneur, ces fichiers seront perdus si le conteneur est supprimé ou même simplement recréé dans Kubernetes. Un système de fichiers éphémère signifie que vous ne pouvez pas vraiment compter sur les données que vous mettez dans ce système de fichiers. Elles ne sont pas persistantes et doivent être traitées comme des données temporaires de courte durée.

# Volumes

Alors, comment obtenir un stockage plus persistant dans nos conteneurs ? Que faisons-nous si notre application a besoin de données qui vivent un peu plus longtemps que votre conteneur Kubernetes moyen, conçu pour être de courte durée ? Eh bien, de nombreuses applications ont besoin d'une méthode de stockage de données plus persistante et les volumes nous permettent d'y parvenir.

Les volumes nous permettent essentiellement de stocker des données en dehors du système de fichiers des conteneurs tout en permettant au conteneur d'accéder aux données en temps réel. Les données peuvent donc persister au-delà de la durée de vie du conteneur, mais le conteneur peut toujours y accéder. En gros, nous stockons nos fichiers en dehors du système de fichiers du conteneur lui-même, dans un emplacement externe, ce qui permet à nos données de persister au-delà de la durée de vie du conteneur. Ainsi, même si le conteneur est supprimé, les données existent toujours dans ce stockage externe.

Les volumes offrent un moyen simple de fournir un stockage externe aux conteneurs au sein de la spécification du conteneur du pod.

# Volumes persistants

Les volumes persistants vont un peu plus loin. Ce sont une forme légèrement plus avancée de volume Kubernetes et ils vous permettent de traiter le stockage comme une ressource abstraite et de la consommer en utilisant vos pods. Nous avons un pod et un conteneur ici, et nous pouvons configurer une revendication de volume persistant (Persistent Volume Claim - PVC), qui se lie ensuite à un volume persistant (Persistent Volume - PV) et le volume persistant implémente une forme de stockage externe, tout comme un volume Kubernetes ordinaire.

Si vous regardez la flèche verte, elle représente essentiellement une couche d'abstraction. Avec les volumes persistants, nous avons une manière légèrement plus complexe, mais aussi plus abstraite, de gérer le stockage externe dans Kubernetes. Ne vous inquiétez pas si cela ne fait pas beaucoup de sens pour le moment, cela deviendra plus clair dans une leçon ultérieure lorsque nous pourrons plonger et examiner comment fonctionnent les volumes persistants dans notre cluster Kubernetes.

# Types de volumes

La dernière chose dont je veux parler dans cette vidéo est le concept de types de volumes. Les volumes et les volumes persistants ont chacun un type de volume, et ce type de volume détermine comment le stockage est réellement géré. Il existe une variété de types de volumes avec toutes sortes de méthodes de stockage différentes que vous pouvez utiliser.

Par exemple, nous pouvons utiliser des volumes NFS pour stocker nos données sur un système de fichiers réseau. Il existe des mécanismes de stockage dans le cloud qui vous permettent de stocker vos données sur des plateformes cloud telles que AWS, Azure et GCP. Nous avons les ConfigMaps et les Secrets, que nous avons déjà utilisés dans ce cours, et qui nous permettent d'exposer des données de configuration sous forme de volume dans un conteneur. Il existe même un type de volume qui nous permet de stocker des données dans un simple répertoire, directement sur le nœud Kubernetes.

Nous parlerons un peu plus des types de volumes que vous devez connaître spécifiquement pour la certification CKA au fur et à mesure que nous avancerons dans cette section.

# Conclusion

Dans cette leçon, nous avons parlé brièvement des systèmes de fichiers des conteneurs et du fait qu'ils sont éphémères. Nous avons parlé du concept de volumes et de la manière dont nous pouvons les utiliser pour stocker nos données en dehors du système de fichiers des conteneurs. Nous avons parlé des volumes persistants, une forme légèrement plus complexe mais aussi plus abstraite de volumes Kubernetes. Enfin, nous avons parlé du concept de types de volumes.

C'est tout pour cette leçon. Je vous verrai dans la prochaine.


# Reférence

https://kubernetes.io/docs/concepts/storage/volumes/

https://kubernetes.io/docs/concepts/storage/persistent-volumes/